import pypsa
import pandas as pd
import numpy as np

# Suppress xarray FutureWarning
import warnings
warnings.filterwarnings("ignore", category=FutureWarning, module="xarray")


data = pd.read_csv('demand-time-series.csv', index_col='date')


def main():
    model = create_building__model(data)
    # solve mnodel
    model.optimize(solver_name='highs')
    # results
    print('#############################')
    print('objective value')
    print(model.objective)
    print('#############################')
    print('power generation')
    print(model.generators_t.p)
    print('#############################')
    print('Power through lossless interconnector')
    print(model.lines_t.p1)
    print('#############################')
    print('Battery power charged')
    print(model.storage_units_t.p_store)
    print('#############################')
    print('Battery power discharged')
    print(model.storage_units_t.p_dispatch)
    print('#############################')
    print('Battery state-of-charge')
    print(model.storage_units_t.state_of_charge)
    print('#############################')
    
    
def create_building__model(data):
    """ creates operational optimization model for supplying heat demand through e-boiler or gas-boiler
    """
    n = pypsa.Network(snapshots=data.index)
    n.add("Bus", 'electricity', carrier='AC')
    n.add("Bus", 'electricity-buildings', carrier='AC')

    # add lossless interconnector
    n.add("Line", 'line',
          bus0='electricity',
          bus1='electricity-buildings',
          s_nom=100, # MW
          x=0.1, # series reactance
          r=0.01, # series resistance
    )
    
    # power from the grid
    n.add("Generator", 'electricity',
      bus='electricity',
      carrier='electricity',
      p_nom=1000,
      marginal_cost=data['e-prices'], # variable electricity prices
    )

    # self generation
    n.add("Generator", 'solar-PV',
      bus='electricity-buildings',
      carrier='electricity',
      p_nom=100, # MW of installed PV capacity
      p_max_pu=data.solar,    
      marginal_cost=0, # self generation
    )

    n.add("StorageUnit","Battery",
          bus='electricity-buildings',
          carrier='AC',
          max_hours=5, # this max hour multiplied by p_nom results in ~1000 MWh energy capacity
          efficiency_store=0.95,
          efficiency_dispatch=0.95,
          p_nom=80, # MW
          state_of_charge_initial=0,
          cyclic_state_of_charge=False, # if True, state_of_charge_initial is ignored and is set to the final state of charge after optimization
    )

    n.add("Load", 'Buildings-demand',
          bus='electricity-buildings',
          carrier='AC',
          p_set=data.load_4.values,
    )
    
    return n


if __name__=='__main__':
    main()