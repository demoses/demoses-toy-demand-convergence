import pypsa
import pandas as pd
import numpy as np

# Suppress xarray FutureWarning
import warnings
warnings.filterwarnings("ignore", category=FutureWarning, module="xarray")


data = pd.read_csv('demand-time-series.csv', index_col='date')


def main():
    model = create_ev_model(data)
    # solve mnodel
    model.optimize(solver_name='highs')
    # results
    print('#############################')
    print('objective value')
    print(model.objective)
    print('#############################')
    print('EV charger power consumption')
    print(model.links_t.p0)
    print('#############################')
    print('EV charger power supply')
    print(model.links_t.p1)
    print('#############################')
    print('EV battery power charged')
    print(model.storage_units_t.p_store)
    print('#############################')
    print('EV battery power discharged')
    print(model.storage_units_t.p_dispatch)
    print('#############################')
    
    
def create_ev_model(data):
    """ creates operational optimization model for supplying heat demand through e-boiler or gas-boiler
    """
    n = pypsa.Network(snapshots=data.index)
    n.add("Bus", 'electricity', carrier='AC')
    n.add("Bus", 'transport', carrier='AC')
    
    n.add("Generator", 'electricity',
      bus='electricity',
      carrier='electricity',
      p_nom=1000,
      marginal_cost=data['e-prices'], # variable electricity prices
    )
    
    n.add("Link","EV_charging",
          bus0="electricity",
          bus1='transport',
          carrier="EV_charger",
          p_nom=400, # MW
          efficiency=0.9,
    )

    n.add("StorageUnit","EV_battery",
          bus='transport',
          carrier='AC',
          max_hours=5, # this max hour multiplied by p_nom results in ~1000 MWh energy capacity
          efficiency_store=0.95,
          efficiency_dispatch=0.95,
          p_nom=300, # MW
          state_of_charge_initial=0,
          cyclic_state_of_charge=False, # if True, state_of_charge_initial is ignored and is set to the final state of charge after optimization
    )


    n.add("Load", 'EV-demand',
          bus='transport',
          carrier='AC',
          p_set=data.load_3.values,
    )
    
    return n


if __name__=='__main__':
    main()