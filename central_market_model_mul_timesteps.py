import pypsa
from pypsa import Network
import linopy
import pandas as pd
import xarray as xr
import numpy as np


# read data
generation_data = pd.read_csv('stepwise-supply-mul-timesteps.csv', index_col='date')
generator_bids = generation_data[:15] # data frame with bid capacity and marginal prices
demand_bids = pd.read_csv('stepwise-demand-mul-timesteps.csv',  index_col='date')
num_hours = 4 # currently a 4 hrs market clearing, should be changed to 24 hrs


def main() -> None:
    # create network 
    network = create_network(generator_bids)
    # create linopy model to be modified
    model = network.optimize.create_model()
    # add demand as variable
    add_dem_as_var_mul_snapshots(model, demand_bids)
    # update bus-nodal-balance-constraint: remove static demand and add variable demand
    update_bus_nodal_balance_cons_mul_snapshots(model)
    # gross surplus
    GrossSurplus = gross_surplus_mul_snapshots(model, demand_bids)
    # production cost 
    ProductionCost = model.objective.expression
    # social welfare
    SocialWelfare = GrossSurplus - ProductionCost 
    # set new objective
    model.objective = SocialWelfare
    model.objective.sense = 'max'
    # solve
    model.solve(solver_name='highs')
    # print solution 
    sw = model.objective.value
    # market price
    market_price = get_market_price(model)
    # demand schedule
    scheduled_demand = get_demand_schedule(model, demand_bids)
    # supply schedule
    scheduled_supply = get_supply_schedule(model)
    print('#############################')
    print(f'social welfare : {sw} €')
    print('#############################')
    print('market price in €/MWh')
    print(market_price) 
    print('#############################')
    print('scheduled demand in MWh')
    print(scheduled_demand)
    print('#############################')
    print('scheduled supply in MWh')
    print(scheduled_supply)
    print('#############################')


def create_network(generator_bids: pd.DataFrame, num_hours=num_hours, dem=20) -> Network:
    """ create pypsa network
    """
    network = pypsa.Network()
    network.add("Bus", "Bus")
    start_hour = 0
    end_hour = start_hour + (num_hours-1)
    network.set_snapshots(pd.date_range(f"2024-01-01 {start_hour:02d}:00", f"2024-01-01 {end_hour:02d}:00", freq="H"))
    
    # add generators
    for index, row in generator_bids.iterrows():
        generator_name = row['generator']
        marginal_cost = row['marg_cost']  
        p_nom = row['capacity']
        
        network.add("Generator",
                generator_name,
                bus='Bus',
                marginal_cost=marginal_cost,
                p_nom=p_nom,
        )
    
    network.add("Load",
                'demand',
                bus='Bus',
                p_set=dem
        )

    return network


def add_dem_as_var_mul_snapshots(model, demand_bids):
    """ creates and add demand as a variable to the model.
    """
    snapshot = model.variables.coords["snapshot"]
    quantity = demand_bids['demand'][:16].values  # currently hard coded with 4 consumers each with 4 bids per timestep (hence, 4*4=16)
    upper = demand_bids['quantity'].values.reshape(4, -1)  # currently hard coded with 4 consumers each with 4 bids per timestep (hence, 4*4=16)
    model.add_variables(name='quantity-supplied', lower=0, upper=upper, coords=(snapshot, quantity), dims=['snapshot', 'quantity'])
    print(model.variables['quantity-supplied'])


def update_bus_nodal_balance_cons_mul_snapshots(model):
    snapshots = model.variables.coords['snapshot']
    new_bus_nodal_balance_cons = []
    for snapshot in snapshots:
        cons = ( model.constraints['Bus-nodal_balance'].sel(snapshot=snapshot).lhs == 
                model.variables['quantity-supplied'].sel(snapshot=snapshot).sum()
        )
        new_bus_nodal_balance_cons.append(cons)
    #remove old bus-nodal_balance constraint
    model.remove_constraints("Bus-nodal_balance")
    # Add new bus-nodal_balance constraint
    for con in new_bus_nodal_balance_cons:
        model.add_constraints(con, name=f"new_bus_nodal_balance_cons-{new_bus_nodal_balance_cons.index(con)}")
    for con in model.constraints:
        print(model.constraints[con])


def gross_surplus_mul_snapshots(model, demand_bids):
    """ creates gross_surplus expression from which total generation cost will be subtracted in the obj. func.
    """
    snapshots = model.variables.coords["snapshot"].values
    coords = {'snapshot' :model.variables.coords["snapshot"],
              'quantity': model.variables.coords["quantity"],
    }
    
    dims = (coords.keys())
    
    # currently hard coded with 4 consumers each with 4 bids per timestep (hence, 4*4=16)
    bid_price_xarray = xr.DataArray(demand_bids['bid-price'].values.reshape(4, -1), coords=coords, dims=dims) 

    LinearExprList = []
    for snapshot in snapshots:
        LinearExpr = (model.variables['quantity-supplied'].sel(snapshot=snapshot) * bid_price_xarray.sel(snapshot=snapshot)).sum()
        LinearExprList.append(LinearExpr)
    GrossSurplus = sum(LinearExprList)
    return GrossSurplus


def get_market_price(model):
    """ returns market clearing price for each snapshot
    """
    market_price = []
    for i in range(len(model.variables.coords['snapshot'])):
        market_price.append(-1*model.constraints[f'new_bus_nodal_balance_cons-{i}'].dual.values.flatten().item())
    market_price_df = pd.DataFrame()
    market_price_df['market_price'] = market_price
    market_price_df.index = model.variables.coords['snapshot']
    market_price_df.index.name = 'snapshots'
    return market_price_df


def get_supply_schedule(model):
    """ returns the production schedule for each generator for each snapshot
    """
    scheduled_supply = pd.DataFrame()
    snapshots = model.variables['Generator-p'].coords['snapshot']
    
    for time, snapshot in enumerate(snapshots):
        scheduled_supply[f'{time}'] = model.solution['Generator-p'].sel(snapshot=snapshot).values
    
    scheduled_supply = scheduled_supply.T
    scheduled_supply.columns = generator_bids['generator']
    scheduled_supply.index = snapshots
    scheduled_supply.index.name = 'snapshots'
    return scheduled_supply


def get_demand_schedule(model, demand_bids):
    """ returns the consumption schedule for each consumer per snapshot
    """
    scheduled_demand = pd.DataFrame()
    snapshots = model.variables['quantity-supplied'].coords['snapshot']
    
    for time, snapshot in enumerate(snapshots):
        scheduled_demand[f'{time}'] = model.solution['quantity-supplied'].sel(snapshot=snapshot).values
    
    scheduled_demand = scheduled_demand.T
    scheduled_demand.index = snapshots
    scheduled_demand.index.name = 'snapshots'
    scheduled_demand.columns = demand_bids['demand'][:16] # hard coded
    return scheduled_demand


if __name__=='__main__':
    main()