# DEMOSES Toy Model repository

This repository contains a toy model for a convergent demand response.

## Installation and Usage

In a (new, virtual) environment of your choice, first install the packages listed in requirements.txt. Then simply run the `manage.py` file, which will print the results after convergence to the terminal

```bash
pip install -r requirements.txt
python manager.py
# ...
# intermediate and final output appear here
# ...
```

Running the manager file will also show a plot that updates after every iteration.
The version after the final iteration is also saved in the top-level directory
as `demand_convergence.png`