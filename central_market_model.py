import pypsa
from pypsa import Network
import linopy
import pandas as pd
import xarray as xr
import numpy as np


# read data
generator_bids = pd.read_csv('stepwise-supply.csv', index_col='num')
demand_bids = pd.read_csv('stepwise-demand.csv',  index_col='num')


def main() -> None:
    # create network 
    network = create_network(generator_bids)
    # create linopy model to be modified
    model = network.optimize.create_model()
    # add demand as variable
    add_dem_as_var(model, demand_bids)
    # update bus-nodal-balance-constraint: remove static demand and add variable demand
    update_bus_nodal_balance_cons(model)
    # gross surplus
    GrossSurplus = gross_surplus(model, demand_bids)
    # production cost 
    ProductionCost = model.objective.expression
    # social welfare
    SocialWelfare = GrossSurplus - ProductionCost 
    # set new objective
    model.objective = SocialWelfare
    model.objective.sense = 'max'
    # solve
    model.solve(solver_name='highs')
    # print solution 
    sw = model.objective.value
    # market price
    market_price = (-1*model.constraints['new_bus_nodal_balance_cons'].dual.values).flatten().item()
    # demand met
    schedule_demand = dict(
                            zip(model.variables['quantity-supplied'].coords['quantity'].values, 
                            model.solution['quantity-supplied'].values.flatten()
                            )
    )
    # generation schedule
    scheduled_supply = dict(
                            zip(model.variables['Generator-p'].coords['Generator'].values, 
                            model.solution['Generator-p'].values.flatten()
                            )
    )
    print('#############################')
    print(f'social welfare : {sw} €')
    print('#############################')
    print(f'market price : {market_price}  €/MWh')
    print('#############################')
    print('scheduled demand: ')
    print(schedule_demand)
    print('#############################')
    print('scheduled supply')
    print(scheduled_supply)
    print('#############################')


def create_network(generator_bids: pd.DataFrame, num_hours=1, dem=20) -> Network:
    """ create pypsa network
    """
    network = pypsa.Network()
    network.add("Bus", "Bus")
    start_hour = 0
    end_hour = start_hour + (num_hours-1)
    network.set_snapshots(pd.date_range(f"2024-01-01 {start_hour:02d}:00", f"2024-01-01 {end_hour:02d}:00", freq="H"))
    
    # add generators
    for index, row in generator_bids.iterrows():
        generator_name = row['generator']
        marginal_cost = row['marg_cost']  
        p_nom = row['capacity']
        
        network.add("Generator",
                generator_name,
                bus='Bus',
                marginal_cost=marginal_cost,
                p_nom=p_nom,
        )
    
    network.add("Load",
                'demand',
                bus='Bus',
                p_set=dem
        )

    return network


def add_dem_as_var(model, demand_bids):
    """ creates and add demand as a variable to the model
    """
    snapshot = model.variables.coords["snapshot"]
    quantity = demand_bids['demand'].values
    upper = [demand_bids['quantity'].values]
    model.add_variables(name='quantity-supplied', lower=0, upper=upper, coords=(snapshot, quantity), dims=['snapshot', 'quantity'])
    print(model.variables['quantity-supplied'])


def update_bus_nodal_balance_cons(model):
    snapshot = model.variables.coords['snapshot']
    new_bus_nodal_balance_cons = (model.constraints['Bus-nodal_balance'].sel(snapshot=snapshot).lhs == 
                                 model.variables['quantity-supplied'].sel(snapshot=snapshot).sum()
    )
    # remove old bus-nodal_balance constraint
    model.remove_constraints("Bus-nodal_balance")
    # Add new bus-nodal_balance constraint
    model.add_constraints(new_bus_nodal_balance_cons, name="new_bus_nodal_balance_cons")
    print(model.constraints['new_bus_nodal_balance_cons'])


def gross_surplus(model, demand_bids):
    """ creates gross_surplus expression from which total generation cost will be subtracted in the obj. func.
    """
    snapshot = model.variables.coords["snapshot"].values
    coords = {'quantity': model.variables.coords["quantity"]}
    dims = list(coords.keys())[0] # convert dict key to list and extract it as string. Final result is: dims = ('quantity',)
    
    bid_price_xarray = xr.DataArray(demand_bids['bid-price'].values, coords=coords, dims=dims)
    GrossSurplus = (model.variables['quantity-supplied'].sel(snapshot=snapshot) * bid_price_xarray).sum()
    # alternatively (not using xarray)
    #expr = model.variables['quantity-supplied'].sel(snapshot=snapshot) * demand_bids['bid-price'].values.tolist()
    
    return GrossSurplus


if __name__=='__main__':
    main()