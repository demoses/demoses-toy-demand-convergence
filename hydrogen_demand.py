import pypsa
import pandas as pd
import numpy as np

# Suppress xarray FutureWarning
import warnings
warnings.filterwarnings("ignore", category=FutureWarning, module="xarray")


data = pd.read_csv('demand-time-series.csv', index_col='date')


def main():
    model = create_hydrogen_model(data)
    # solve mnodel
    model.optimize(solver_name='highs')
    # results
    print('#############################')
    print('objective value')
    print(model.objective)
    print('#############################')
    print('electrolyzer power consumption')
    print(model.links_t.p0)
    print('#############################')
    print('electrolyzer power supply')
    print(model.links_t.p1)
    print('#############################')


    
def create_hydrogen_model(data):
    """ creates operational optimization model for supplying hydrogen demand through electrolysis 
    """
    n = pypsa.Network(snapshots=data.index)
    n.add("Bus", 'electricity', carrier='AC')
    n.add("Bus", 'hydrogen', carrier='hydrogen')
    
    n.add("Generator", 'electricity',
      bus='electricity',
      carrier='electricity',
      p_nom=1000,
      marginal_cost=data['e-prices'], # variable electricity prices
    )
    
    n.add("Link","Electrolysis",
          bus0='electricity',
          bus1='hydrogen',
          carrier="Electrolyzer",
          p_nom=500, # MW capacity
          efficiency=0.6,
    )

    n.add("Store", 'H2_storage', # operated without deadline constraints on energy level
          bus='hydrogen',
          carrier='hydrogen',
          e_nom=data.load_1.sum().sum()*0.5, # in MWh, 50% of its demand can be buffered
          e_initial=0,
    )

    n.add("Load", 'H2-demand',
          bus='hydrogen',
          carrier='Hydrogen',
          p_set=data.load_1.values,
    )
    return n


if __name__=='__main__':
    main()