import pypsa
import pandas as pd
import numpy as np

# Suppress xarray FutureWarning
import warnings
warnings.filterwarnings("ignore", category=FutureWarning, module="xarray")


data = pd.read_csv('demand-time-series.csv', index_col='date')


def main():
    model = create_heat_model(data)
    # solve mnodel
    model.optimize(solver_name='highs')
    # results
    print('#############################')
    print('objective value')
    print(model.objective)
    print('#############################')
    print('heat tech power consumption')
    print(model.links_t.p0)
    print('#############################')
    print('heat tech power supply')
    print(model.links_t.p1)
    print('#############################')


    
def create_heat_model(data):
    """ creates operational optimization model for supplying heat demand through e-boiler or gas-boiler
    """
    n = pypsa.Network(snapshots=data.index)
    n.add("Bus", 'electricity', carrier='AC')
    n.add("Bus", 'heat', carrier='heat')
    n.add("Bus", 'methane', carrier='methane')
    
    n.add("Generator", 'electricity',
      bus='electricity',
      carrier='electricity',
      p_nom=1000,
      marginal_cost=data['e-prices'], # variable electricity prices
    )
    
    n.add("Link","E-boiler",
          bus0='electricity',
          bus1='heat',
          carrier="heat",
          p_nom=300, # MW
          efficiency=0.8,
    )

    n.add("Link","gas-boiler",
          bus0='methane',
          bus1='heat',
          carrier='heat',
          p_nom=600, # MW
          efficiency=0.8,
    )

    n.add("Store", 'Methane_store', # operated without deadline constraints on energy level
       bus='methane',
       carrier='methane',
       e_nom=1000e6, # almost infinite
       e_initial=1000e6, # should not be depleted
       marginal_cost=data['g-prices'], # for now, assume constant methane price
    )

    n.add("Store", 'Heat_storage',
          bus='heat',
          carrier='heat',
          e_nom=data.load_2.sum().sum()*0.5, # in MWh, 50% of its demand can be buffered
          e_initial=0,
    )

    n.add("Load", 'heat-demand',
          bus='heat',
          carrier='heat',
          p_set=data.load_2.values,
    )
    
    return n


if __name__=='__main__':
    main()